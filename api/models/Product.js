const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');

const ProductSchema = new mongoose.Schema({
	title: {
		type: String,
		required: true,
	},
	price: {
		type: Number,
		required: true,
	},
	description: {
		type: String,
		required: true,
	},
	image: {
		type: String,
		required: true,
	},
	category: {
		type: String,
		required: true
	},
	salesman: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
});

ProductSchema.plugin(idvalidator);
const Product = mongoose.model('Product', ProductSchema);
module.exports = Product;