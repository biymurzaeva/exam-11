const express = require('express');
const path = require('path');
const multer = require("multer");
const {nanoid} = require("nanoid");
const Product = require('../models/Product');
const config = require('../config');
const auth = require("../middleware/auth");

const router = express.Router();

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

router.get('/', async (req, res) => {
	try {
		const products = await Product.find();
		res.send(products);
	} catch (error) {
		res.sendStatus(500);
	}
});

router.get('/:id', async (req, res) => {
	try {
		const product = await Product.findById(req.params.id);

		if (product) {
			res.send(product);
		} else {
			res.status(404).send({error: 'Product not found'});
		}

	} catch (error) {
		res.sendStatus(500);
	}
});

router.post('/', auth, upload.single('image'), async (req, res) => {
	if (!req.body.title || !req.body.price || !req.body.description || !req.body.category) {
		return res.status(400).send({error: 'Data not valid'});
	}

	const productData = {
		title: req.body.title,
		price: req.body.price,
		image: req.file.filename,
		description: req.body.description,
		category: req.body.category,
		salesman: req.user._id,
	};

	const product = new Product(productData);

	try {
		await product.save();
		res.send(product);
	} catch {
		res.status(400).send({error: 'Data not valid'});
	}
});

router.delete('/:id', auth, async (req, res) => {
	const product = await Product.findById(req.params.id);

	if (!product) return res.status(404).send({error: 'Product not found'});

	if (req.user._id.toString() !== product.salesman._id.toString()) {
		return res.status(403).send({error: 'Permissions denied'});
	}

	try {
		const product = await Product.findByIdAndRemove(req.params.id);
		res.send(`Product ${product.title} removed`);
	} catch (e) {
		res.sendStatus(500);
	}
});

module.exports = router;
