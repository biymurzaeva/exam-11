import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AddProduct from "./containers/AddProguct/AddProduct";
import Products from "./containers/Products/Products";
import Product from "./containers/Product/Product";

const App = () => (
	<Layout>
		<Switch>
			<Route path='/' exact component={Products}/>
			<Route path='/register' component={Register}/>
			<Route path='/login' component={Login}/>
			<Route path='/add-product' component={AddProduct}/>
			<Route path='/products/:id' component={Product}/>
		</Switch>
	</Layout>
);

export default App;
