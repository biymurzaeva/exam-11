import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Router} from "react-router-dom";
import {Provider} from "react-redux";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {createTheme, MuiThemeProvider} from "@material-ui/core";
import history from "./history";
import usersReducer from "./store/reducers/usersReducer";
import productsReducer from "./store/reducers/productsReducer";

const saveToLocalStorage = state => {
	try {
		const serializedState = JSON.stringify(state);
		localStorage.setItem('appState', serializedState);
	} catch (e) {
		console.log('Could not save state');
	}
};

const loadFromLocalStorage = () => {
	try {
		const serializedState = localStorage.getItem('appState');

		if (serializedState === null) {
			return undefined;
		}

		return JSON.parse(serializedState);
	} catch (e) {
		return undefined;
	}
};

const rootReducer = combineReducers({
	users: usersReducer,
	products: productsReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(
	rootReducer,
	persistedState,
	composeEnhancers(applyMiddleware(thunk)),
);

store.subscribe(() => {
	saveToLocalStorage({
		users: store.getState().users,
	});
});

const theme = createTheme({
	props: {
		MuiTextField: {
			variant: "outlined",
			fullWidth: true,
		},
	},
});

const app = (
	<Provider store={store}>
		<Router history={history}>
			<MuiThemeProvider theme={theme}>
				<App/>
			</MuiThemeProvider>
		</Router>
	</Provider>
);

ReactDOM.render(app, document.getElementById('root'));
