import React, {useEffect} from 'react';
import {Box, Button, CircularProgress, Grid, Paper, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {deleteProduct, getProduct} from "../../store/actions/productsActions";
import {apiURL} from "../../config";

const Product = ({match, history}) => {
	const dispatch = useDispatch();
	const product = useSelector(state => state.products.product);
	const productLoading = useSelector(state => state.products.productLoading);
	const law = useSelector(state => state.products.law);

	useEffect(() => {
		dispatch(getProduct(match.params.id));
	}, [dispatch, match.params.id]);

	const deleteOwnProduct = async id => {
		await dispatch(deleteProduct(id));
		history.replace('/');
	};

	let btn = (
		<></>
	);

	if (law === true) {
		btn = (
			<Button onClick={() => deleteOwnProduct(product._id)}>Delete</Button>
		);
	}

	return (
		<>
			{
				productLoading ?
				<Grid container justifyContent="center" alignItems="center">
					<Grid item>
						<CircularProgress/>
					</Grid>
				</Grid> :
				<Paper component={Box} p={2}>
					<img src={apiURL + '/uploads/' + product.image} alt="Product" width="200px"/>
					<Typography variant="h4">{product.title}</Typography>
					<Typography variant="subtitle2">{product.price}</Typography>
					<Typography variant="body1">{product.description}</Typography>
					{btn}
				</Paper>
			}
		</>
	);
};

export default Product;