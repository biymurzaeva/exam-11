import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getProducts} from "../../store/actions/productsActions";
import {CircularProgress, Grid} from "@material-ui/core";
import ProductItem from "../../components/ProductItem/ProductItem";

const Products = () => {
	const dispatch = useDispatch();
	const products = useSelector(state => state.products.products);
	const productLoading = useSelector(state => state.products.productsLoading);

	useEffect(() => {
		dispatch(getProducts());
	}, [dispatch]);

	return (
		<Grid item>
			<Grid item container direction="row" spacing={1}>
				{productLoading ? (
					<Grid container justifyContent="center" alignItems="center">
						<Grid item>
							<CircularProgress/>
						</Grid>
					</Grid>
				) : products.map(product => (
					<ProductItem
						key={product._id}
						id={product._id}
						title={product.title}
						price={product.price}
						image={product.image}
					/>
				))}
			</Grid>
		</Grid>
	);
};

export default Products;