import React from 'react';
import {useDispatch} from "react-redux";
import ProductForm from "../../components/ProductForm/ProductForm";
import {createProduct} from "../../store/actions/productsActions";

const AddProduct = ({history}) => {
	const dispatch = useDispatch();

	const onSubmit = async postData => {
		await dispatch(createProduct(postData));
		history.replace('/');
	};

	return (
		<>
			<ProductForm onSubmit={onSubmit}/>
		</>
	);
};

export default AddProduct;