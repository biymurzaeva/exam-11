import axiosApi from "../../axiosApi";

export const CREATE_PRODUCT_REQUEST = 'CREATE_PRODUCT_REQUEST';
export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';
export const CREATE_PRODUCT_FAILURE = 'CREATE_PRODUCT_FAILURE';

export const GET_PRODUCTS_REQUEST = 'GET_PRODUCTS_REQUEST';
export const GET_PRODUCTS_SUCCESS = 'GET_PRODUCTS_SUCCESS';
export const GET_PRODUCTS_FAILURE = 'GET_PRODUCTS_FAILURE';

export const GET_PRODUCT_REQUEST = 'GET_PRODUCT_REQUEST';
export const GET_PRODUCT_SUCCESS = 'GET_PRODUCT_SUCCESS';
export const GET_PRODUCT_FAILURE = 'GET_PRODUCT_FAILURE';

export const DELETE_PRODUCT_REQUEST = 'DELETE_PRODUCT_REQUEST';
export const DELETE_PRODUCT_SUCCESS = 'DELETE_PRODUCT_SUCCESS';
export const DELETE_PRODUCT_FAILURE = 'DELETE_PRODUCT_FAILURE';

export const GET_LAW = 'GET_LAW';

export const getLaw = law => ({type: GET_LAW, payload: law});

export const createProductRequest = () => ({type: CREATE_PRODUCT_REQUEST});
export const createProductSuccess = () => ({type: CREATE_PRODUCT_SUCCESS});
export const createProductFailure = error => ({type: CREATE_PRODUCT_FAILURE, payload: error});

export const getProductsRequest = () => ({type: GET_PRODUCTS_REQUEST});
export const getProductsSuccess = products => ({type: GET_PRODUCTS_SUCCESS, payload: products});
export const getProductsFailure = error => ({type: GET_PRODUCTS_FAILURE, payload: error});

export const getProductRequest = () => ({type: GET_PRODUCT_REQUEST});
export const getProductSuccess = product => ({type: GET_PRODUCT_SUCCESS, payload: product});
export const getProductFailure = error => ({type: GET_PRODUCT_FAILURE, payload: error});

export const deleteProductRequest = () => ({type: DELETE_PRODUCT_REQUEST});
export const deleteProductSuccess = () => ({type: DELETE_PRODUCT_SUCCESS});
export const deleteProductFailure = error => ({type: DELETE_PRODUCT_FAILURE, payload: error});

export const createProduct = productData => {
	return async (dispatch, getState) => {
		const headers = {
			'Authorization': getState().users.user && getState().users.user.token
		};

		try {
			dispatch(createProductRequest());
			await axiosApi.post('/products', productData, {headers});
			dispatch(createProductSuccess());
		} catch (error) {
			dispatch(createProductFailure(error));
		}
	};
};

export const getProducts = () => {
	return async dispatch => {
		try {
			dispatch(getProductsRequest());
			const response = await axiosApi.get('/products');
			dispatch(getProductsSuccess(response.data));
		} catch (error) {
			dispatch(getProductsFailure(error));
		}
	};
};

export const getProduct = id => {
	return async (dispatch, getState) => {
		try {
			dispatch(getProductRequest());
			const response = await axiosApi.get(`/products/${id}`);

			if (response.data['salesman'] === getState().users.user._id) {
				dispatch(getLaw(true));
			}

			dispatch(getProductSuccess(response.data));
		} catch (error) {
			dispatch(getProductFailure(error));
		}
	};
};

export const deleteProduct = id => {
	return async (dispatch, getState) => {
		const headers = {
			'Authorization': getState().users.user && getState().users.user.token
		};

		try {
			dispatch(deleteProductRequest());
			await axiosApi.delete('/products/' + id, {headers});
		} catch (error) {
			dispatch(deleteProductFailure(error));
		}
	};
};