import {
	CREATE_PRODUCT_FAILURE,
	CREATE_PRODUCT_REQUEST,
	CREATE_PRODUCT_SUCCESS, DELETE_PRODUCT_FAILURE, DELETE_PRODUCT_REQUEST, DELETE_PRODUCT_SUCCESS, GET_LAW,
	GET_PRODUCT_FAILURE,
	GET_PRODUCT_REQUEST,
	GET_PRODUCT_SUCCESS,
	GET_PRODUCTS_FAILURE,
	GET_PRODUCTS_REQUEST,
	GET_PRODUCTS_SUCCESS
} from "../actions/productsActions";

const initialState = {
	createProductLoading: false,
	createProductError: null,
	products: [],
	productsLoading: false,
	productError: null,
	productLoading: false,
	error: null,
	product: {},
	deleteError: null,
	deleteLoading: false,
	law: false,
};

const productsReducer = (state = initialState, action) => {
	switch (action.type) {
		case CREATE_PRODUCT_REQUEST:
			return {...state, createProductLoading: true};
		case CREATE_PRODUCT_SUCCESS:
			return {...state, createProductLoading: false, createProductError: null};
		case CREATE_PRODUCT_FAILURE:
			return {...state, createProductLoading: false, createProductError: action.payload};
		case GET_PRODUCTS_REQUEST:
			return {...state, productsLoading: true};
		case GET_PRODUCTS_SUCCESS:
			return {...state, productsLoading: false, products: action.payload};
		case GET_PRODUCTS_FAILURE:
			return {...state, productsLoading: false, productError: action.payload};
		case GET_PRODUCT_REQUEST:
			return {...state, productLoading: true};
		case GET_PRODUCT_SUCCESS:
			return {...state, productLoading: false, product: action.payload};
		case GET_LAW:
			return {...state, law: action.payload};
		case GET_PRODUCT_FAILURE:
			return {...state, productLoading: false, error: action.payload};
		case DELETE_PRODUCT_REQUEST:
			return {...state, deleteLoading: true};
		case DELETE_PRODUCT_SUCCESS:
			return {...state, deleteLoading: false, deleteError: null};
		case DELETE_PRODUCT_FAILURE:
			return {...state, deleteLoading: false, deleteError: action.payload};
		default:
			return state;
	}
};

export default productsReducer;